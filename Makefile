.PHONY: fresh build release
.DEFAULT_GOAL := fresh

fresh: build clean

build:
	cp -r ./scripts/ ./build-20/
	$(MAKE) -C ./build-20/ build

release: build
	$(MAKE) -C ./build-20/ release_latest

clean:
	rm -r ./build-20/scripts

