#!/bin/bash

export LC_ALL=C
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:.
RPO_DIR="${BITBUCKET_CLONE_DIR}/apo/${BITBUCKET_BUILD_NUMBER}/"
ERROR=0

echo
echo "Artifacts directory -> " ${RPO_DIR}
echo

# Move os arquivos para dentro do projeto, para serem disponibilizados como artifatcs.
mkdir -p "${RPO_DIR}"
echo ${BITBUCKET_BUILD_NUMBER} > "${RPO_DIR}"build_number.txt
mv /protheus12/apo/custom/* "${RPO_DIR}" || ERROR=$?

# Verifica se o RPO foi movido com sucesso.
if [[ $ERROR -eq 0 ]]; then
	ls -lh ${RPO_DIR}

	echo "*********************************"
	echo "*   RPO deployed successfully   *"
	echo "*********************************"
else
	if [[ $ERROR -eq 0 ]]; then
		$ERROR=1
	fi
	echo "RPO ** NOT ** deployed properly - error $ERROR"
	exit $ERROR
fi
