#!/bin/bash

export LC_ALL=C
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:.
OUTREPORT="/protheus12/apo/"
FILE_ERROR="${OUTREPORT}"defragrpo_errors.log
ERROR=0

# Cria diretórios para os RPO.
mkdir /protheus12/apo/custom/ -p
mkdir /protheus12/apo/standard/ -p

# Desfragmenta o RPO.
cd /protheus12/bin/appserver/
./appsrvlinux -compile -env=P12 -defragrpo -outreport="${OUTREPORT}" || ERROR=$?

# Verifica se a desfragmentação ocorreu com sucesso.
if [[ $ERROR -eq 0 && -f "${FILE_ERROR}" && ! -s "${FILE_ERROR}" ]]; then
	echo "**************************************"
	echo "*   RPO desfragmented successfully   *"
	echo "**************************************"
else
	if [[ $ERROR -eq 0 ]]; then
		$ERROR=1
	fi
	if [[ -f "${FILE_ERROR}" && -s "${FILE_ERROR}" ]]; then
		cat "${FILE_ERROR}"
	fi
	echo "RPO ** NOT ** desfragmented properly - error $ERROR"
	exit $ERROR
fi
