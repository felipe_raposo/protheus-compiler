#!/bin/bash

export LC_ALL=C
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:.
FULL_PATCHES_PATH="${BITBUCKET_CLONE_DIR}$1"

# Cria diretórios para os RPO.
mkdir /protheus12/apo/custom/ -p
mkdir /protheus12/apo/standard/ -p

# Aplica patches.
if [[ ! -z ${1+x} && -d "${FULL_PATCHES_PATH}" ]]; then
	OUTREPORT="/protheus12/apo/"
	FILE_ERROR="${OUTREPORT}"patch_errors.log
	ERROR=0

	echo
	echo "Patches path -> ${FULL_PATCHES_PATH}"
	echo

	for patch_file in "${FULL_PATCHES_PATH}"/*.ptm
	do
		if [[ -f "${patch_file}" ]]; then
			echo "Applying patch ${patch_file}"
			cd /protheus12/bin/appserver/
			./appsrvlinux -compile -applypatch -env=P12 -files="${patch_file}" -outreport="${OUTREPORT}" || ERROR=$?

			if [[ $ERROR -ne 0 ]]; then
				break
			fi
		fi
	done

	# Verifica se a aplicação das patches ocorreu com sucesso.
	if [[ $ERROR -eq 0 && -f "${FILE_ERROR}" && ! -s "${FILE_ERROR}" ]]; then
		echo "************************************"
		echo "*   Patches applied successfully   *"
		echo "************************************"
	else
		if [[ $ERROR -eq 0 ]]; then
			$ERROR=1
		fi
		if [[ -f "${FILE_ERROR}" && -s "${FILE_ERROR}" ]]; then
			cat "${FILE_ERROR}"
		fi
		echo "Patches ** NOT ** applied properly - error $ERROR"
		exit $ERROR
	fi
else
	echo "No patch to apply"
fi
