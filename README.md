# Protheus Compiler

Esse projeto visa montar um container Docker usado para compilação do projeto do Protheus.

No seu projeto BitBucket, você pode montar o arquivo bitbucet-pipelines.yml, como no exemplo abaixo:

```bash
# Image: feliperaposo/protheus-compiler (versão 20 para Harpia)
# Project: https://bitbucket.org/felipe_raposo/protheus-compiler/
# - Essa imagem compila todo o projeto e desfragmenta o RPO, e disponibiliza o RPO e os logs gerados no processo como artefatos.
# Prerequisites:
# - $SOURCE_PATH    - caminho relativo de onde se encontram os fontes a ser compilados.
# - $INCLUDE_TOTVS  - caminho relativo das includes padrões da TOTVS.
# - $INCLUDE_CUSTOM - caminho relativo das includes específicas do projeto.

# Image: atlassian/bitbucket-upload-file
# Project: https://bitbucket.org/atlassian/bitbucket-upload-file/
# - Essa imagem disponibiliza os artefatos (RPO e logs) na área de downloads.
# Prerequisites:
# - $BITBUCKET_USERNAME     - usuário Bitbucket usado para disponibilizar os artefatos na área de downloads.
# - $BITBUCKET_APP_PASSWORD - App password do usuário $BITBUCKET_USERNAME. Deve ter acesso a gravação no repositório.

clone:
  lfs: true

pipelines:
  branches:
    master:
      - parallel:
        - step:
            image: feliperaposo/protheus-compiler:20
            name: Build RPO Harpia
            script:
              - /apply_patch.sh "/patches/20"
              - /build.sh
              - /defrag.sh
              - /artifacts.sh
            artifacts:
              - apo/**
      - step:
          name: Upload artifacts
          script:
            - pipe: atlassian/bitbucket-upload-file:0.3.2
              variables:
                BITBUCKET_USERNAME: $BITBUCKET_USERNAME
                BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
                FILENAME: "${BITBUCKET_CLONE_DIR}/apo/${BITBUCKET_BUILD_NUMBER}/*.*"
```
